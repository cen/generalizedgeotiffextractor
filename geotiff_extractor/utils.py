from functools import lru_cache
from pathlib import Path
from typing import Any, Callable, Union

import pyproj
import shapely.ops
from enpgt_task_file_reader import PolygonData, convert_nsv_task_file_to_polygons
from enpgt_task_file_reader import settings as task_file_reader_settings
from pyproj import Proj, Transformer
from shapely import MultiPolygon, Polygon

from geotiff_extractor import settings

CRS_WGS84 = Proj("WGS84")
TRANSFORMER_CACHE: dict[tuple[str, str], Transformer]


def print_polygon(polygon: Union[Polygon, MultiPolygon]) -> None:
    for coord in polygon.exterior.coords:
        print("{" + f"lat: {coord[1]}, lng: {coord[0]}" + "},")

    print("---")


_messages = set()


def print_once(key: str, message: str) -> None:
    if key not in _messages:
        print(message)
    _messages.add(key)


def reset_messages() -> None:
    _messages.clear()


def pprint_float(x: Union[float, int], keep_decimal_point: bool = True) -> str:
    removed_zeros = f"{x:.5f}".rstrip("0")
    if not removed_zeros.endswith("."):
        return removed_zeros

    if keep_decimal_point:
        return removed_zeros + "0"

    return removed_zeros.rstrip(".")


def abridged_set_of_user_attributes(obj: Any) -> list[str]:
    return [
        token
        for token in set(dir(obj)) - set(dir(type("dummy", (object,), {})))
        if not (token.startswith("__") and token.endswith("__"))
    ]


@lru_cache(maxsize=10)
def parse_task_file(path: Path, radius: int) -> dict[str, PolygonData]:
    task_file_reader_settings.multiprocessing_cores = settings.multiprocessing_cores
    if settings.task_file_reader_polygon_resolution is not None:
        task_file_reader_settings.circle_resolution = settings.task_file_reader_polygon_resolution

    return convert_nsv_task_file_to_polygons(path, radius)


def projected_polygon_data(polygon_data: PolygonData, crs: pyproj.Proj) -> PolygonData:
    if crs == CRS_WGS84:
        return polygon_data

    transproj = cached_transproj(CRS_WGS84, crs)
    transposed_area_poly = shapely.ops.transform(transproj.transform, polygon_data.polygon)
    transposed_x, transposed_y = polygon_data.original_x, polygon_data.original_y
    if transposed_x is not None and transposed_y is not None:
        transposed_x, transposed_y = transproj.transform(transposed_x, transposed_y)

    return PolygonData(
        transposed_area_poly,
        transposed_x,
        transposed_y,
        polygon_data.radius,
    )


class CacheProjs:
    def __init__(self, function: Callable[[Proj, Proj], Transformer]):
        self.cache: dict[tuple[str, str], Transformer] = {}
        self.function = function

    def __call__(self, proj1: Proj, proj2: Proj) -> Transformer:
        srs_tuple = (proj1.srs, proj2.srs)
        if srs_tuple not in self.cache:
            self.cache[srs_tuple] = self.function(proj1, proj2)
        if len(self.cache) > 10:
            self.cache.pop(next(iter(self.cache)))
        return self.cache[srs_tuple]


@CacheProjs
def cached_transproj(proj1: Proj, proj2: Proj) -> Transformer:
    return Transformer.from_proj(proj1, proj2, always_xy=True)
