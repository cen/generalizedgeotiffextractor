from dataclasses import dataclass
from logging import error
from pathlib import Path
from typing import Any, Optional, Union

import numpy as np
import pyproj
import zarr
from geotiff import GeoTiff
from geotiff.geotiff import UserDefinedGeoKeyError
from tifffile import TiffFile

import geotiff_extractor.settings as settings


@dataclass(frozen=True)
class GeoTiffData:
    path: Path
    value_array: Union[np.ndarray, zarr.core.Array]
    pixel_width: float
    pixel_height: float
    top_left_x_center: float
    top_left_y_center: float
    bottom_right_x_center: float
    bottom_right_y_center: float
    crs: pyproj.Proj
    dtype: np.dtype
    nodata_value: Any

    def pixels_are_invalid(self, pixels: np.ndarray) -> np.ndarray:
        if self.nodata_value is None:
            return np.zeros_like(pixels)
        if self.nodata_value == "floatmin":
            return pixels <= np.finfo(self.dtype).min / 10
        return np.equal(pixels, self.nodata_value)

    def pixel_is_invalid(self, pixel: float | int) -> bool:
        if self.nodata_value is None:
            return False
        if self.nodata_value == "floatmin":
            return bool(pixel <= np.finfo(self.dtype).min / 10)
        return pixel == self.nodata_value


def infer_default_nodata_value_for_dtype(dtype: np.dtype) -> Optional[Any]:
    if dtype is None:
        return None
    if np.issubdtype(dtype, np.integer):
        return np.iinfo(dtype).max
    if np.issubdtype(dtype, np.floating):
        # For floating point, -99999 and something close to the minimal float are the most common invalid values
        return "floatmin"
    return None


def build_tfw_tiffile(tif_file_path: Path, crs: Optional[pyproj.Proj] = None) -> GeoTiffData:
    tif_file = TiffFile(tif_file_path)
    store = tif_file.aszarr()
    value_array = zarr.open(store, mode="r")
    if value_array.dtype.itemsize * value_array.shape[0] * value_array.shape[1] < settings.tif_size_to_load_into_ram:
        value_array = value_array[:]

    y_size, x_size = value_array.shape

    tfw_file_path = tif_file_path.with_suffix(".tfw")
    with open(tfw_file_path) as tfw_file:
        lines = [line.strip() for line in tfw_file.readlines() if line.strip()]

    pixel_width = float(lines[0])
    pixel_height = float(lines[3])

    top_left_x_center = float(lines[4])
    top_left_y_center = float(lines[5])
    bottom_right_x_center = top_left_x_center + pixel_width * (x_size - 1)
    bottom_right_y_center = top_left_y_center - pixel_width * (y_size - 1)

    if crs is None:
        raise ValueError("Need to provide crs for .tfw geotiffs as an argument. Example crs=EPSG:3035")

    dtype = tif_file.pages[0].dtype
    if dtype is None:
        raise RuntimeError("dtype of tif array was None, somehow.")

    assert pixel_width > 0 > pixel_height, "Expecting pixel width to be positive and pixel height to be negative."

    assert settings.classes is None or np.issubdtype(dtype, np.integer), "Classes can only be used with int geotiffs"

    return GeoTiffData(
        tif_file_path,
        value_array,
        pixel_width,
        -pixel_height,
        top_left_x_center,
        top_left_y_center,
        bottom_right_x_center,
        bottom_right_y_center,
        crs,
        dtype,
        infer_default_nodata_value_for_dtype(dtype),
    )


def build_geo_tiffile(tif_file_path: Path, crs: Optional[pyproj.Proj] = None) -> GeoTiffData:
    if not crs:
        try:
            geo_tiff = GeoTiff(str(tif_file_path))
        except UserDefinedGeoKeyError:
            raise RuntimeError(
                "Could not automatically parse CRS from geotiff file."
                " Please provide it as an argument. Example crs=EPSG:3035"
            )
        crs = pyproj.Proj(geo_tiff.crs_code)

    geo_tiff = GeoTiff(str(tif_file_path), crs_code=crs.crs.to_epsg(), as_crs=crs.crs.to_epsg())
    value_array = geo_tiff.read()
    nodata_value = value_array.fill_value
    if value_array.dtype.itemsize * value_array.shape[0] * value_array.shape[1] < settings.tif_size_to_load_into_ram:
        value_array = value_array[:]

    top_left_x_center, top_left_y_center = tuple(
        a + (b - a) / 2 for a, b in zip(geo_tiff.get_coords(1, 1), geo_tiff.get_coords(0, 0))
    )

    y_max, x_max = geo_tiff.tif_shape

    bottom_right_x_center, bottom_right_y_center = tuple(
        a + (b - a) / 2 for a, b in zip(geo_tiff.get_coords(x_max, y_max), geo_tiff.get_coords(x_max - 1, y_max - 1))
    )

    pixel_width, pixel_height = tuple(
        (a - b) for a, b in zip(geo_tiff.get_coords(x_max, y_max), geo_tiff.get_coords(x_max - 1, y_max - 1))
    )

    dtype = value_array.dtype

    if settings.nodata_override is not None:
        nodata_value = settings.nodata_override

    elif nodata_value == 0:
        nodata_value = infer_default_nodata_value_for_dtype(dtype)
        if nodata_value is None:
            error("GDAL nodata value was invalid. Assuming there is no nodata value.")
        else:
            error(f"GDAL nodata value was invalid. Assuming {nodata_value} as the nodata value.")

    assert pixel_width > 0 > pixel_height, "Expecting pixel width to be positive and pixel height to be negative."

    assert settings.classes is None or np.issubdtype(dtype, np.integer), "Classes can only be used with int geotiffs"

    return GeoTiffData(
        tif_file_path,
        value_array,
        pixel_width,
        -pixel_height,
        top_left_x_center,
        top_left_y_center,
        bottom_right_x_center,
        bottom_right_y_center,
        crs,
        dtype,
        nodata_value,
    )


def build_tiffile(tif_file_path: Path, crs: Optional[pyproj.Proj] = None) -> GeoTiffData:
    tfw_file_path = tif_file_path.with_suffix(".tfw")
    if tfw_file_path.is_file():
        return build_tfw_tiffile(tif_file_path, crs)
    else:
        return build_geo_tiffile(tif_file_path, crs)
