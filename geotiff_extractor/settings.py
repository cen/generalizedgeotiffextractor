from typing import Optional

invalid_as_0 = False
# Treat "no data" as 0.
# Example: A tree dataset might define bodies of water as "invalid data".
# However, there is an argument that it should be 0, since there are no trees on water

fast_mode_pixels = 1000
# The minimum amount of pixels when "fast mode" will be used instead of "strict mode".
# In strict mode, each pixel will actually be cut to just the part of it that falls within the radius.
# This is especially important when the pixels are very large and only a few of them fall within your search areas.
# In fast mode, it is just determined whether the pixel center falls within there. If yes, the whole pixel is used.
# Especially in the case of equal area projections, this yields enormous performance benefits.

multiprocessing_cores = 0
# Enable multiprocessing with as many cores as specified.
# 0 / None = Don't use multiprocessing
# -1 = Use multiprocessing with system default

tif_size_to_load_into_ram = 100000000
# Size in bytes to load into RAM as a numpy array rather than a z_array.
# Improves performance, but obviously you need enough RAM, and you need it per multiprocessing thread.

treat_value_as_per_n_square_meters = 0
# Divide each value by pixel area first (an then still use area as weight)
# Example: Using an "absolute population per pixel" dataset as a population density dataset.

nodata_override: Optional[int] = None
# Some geotiff files are shipped with invalid nodata values.
# In those cases, this tool will infer it from the dtype of the pixel values.
# However, this might produce an incorrect result and will also output a warning.
# This value can be used to set a custom nodata value.

classes: Optional[dict[int, str]] = None
# Some geotiffs might have the pixel values refer to classes instead of being a numerical scale.
# You can provide those classes here.

task_file_reader_polygon_resolution = None
# Specifies the resolution for the circle polygons. Corresponds to the quad_segs argument of shapely.buffer.
# Passthrough arg for the task file reader. If not set here, the default from the task file reader will be used.
