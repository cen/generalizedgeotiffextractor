import ast
import os
import shutil
import sys
import traceback
from dataclasses import replace
from datetime import datetime
from logging import error
from multiprocessing import Pool
from pathlib import Path
from typing import Any, Optional, Union

from enpgt_task_file_reader import PolygonData
from pyproj import Proj
from tqdm import tqdm

from geotiff_extractor import settings
from geotiff_extractor.geotiff_extraction import extract_single_value_poly
from geotiff_extractor.results_dataclasses import ExactValueResult, MultiValueTifResult, SingleTifResult
from geotiff_extractor.tif_dataclasses import GeoTiffData, build_tiffile
from geotiff_extractor.utils import abridged_set_of_user_attributes, parse_task_file, reset_messages


def extract_single_value_poly_star(star: tuple[Optional[GeoTiffData], str, PolygonData]) -> SingleTifResult:
    star_list = list(star)
    if star_list[0] is None:
        star_list[0] = getattr(settings, "tif_file_passthrough")

    try:
        return extract_single_value_poly(*star_list)
    except Exception as e:
        error(
            f"Could not extract data for line. Geo ID: {star_list[1]}. "
            f'Reason:\n"{e if isinstance(e, AssertionError) else traceback.format_exc()}".'
        )
    if isinstance(star[2].radius == 0):
        return ExactValueResult(star_list[1])
    else:
        return MultiValueTifResult(star_list[1])


def multiprocessing_initialize(settings_dict: dict[str, Any], tif_file_path: Path, crs: Proj) -> None:
    for setting, old_value in settings_dict.items():
        setattr(settings, setting, old_value)

    setattr(settings, "tif_file_passthrough", build_tiffile(tif_file_path, crs))


def extract_data_from_prepared_geotiff(tif_file: GeoTiffData, infile: Path, radius: int) -> list[SingleTifResult]:
    print("---")
    print(f"Getting stats for file {infile} with radius {radius}m.")

    reset_messages()

    output = list()

    geo_id_to_polygon = parse_task_file(infile, radius)

    polygon_data_to_geo_id = {polygon: geo_id for geo_id, polygon in geo_id_to_polygon.items()}

    if settings.multiprocessing_cores:
        starmap = ((None, geo_id, polygon_data) for polygon_data, geo_id in polygon_data_to_geo_id.items())

        settings_vals = {setting: getattr(settings, setting) for setting in abridged_set_of_user_attributes(settings)}

        if settings.multiprocessing_cores == -1:
            with Pool(
                initializer=multiprocessing_initialize,
                initargs=(settings_vals, tif_file.path, tif_file.crs),
            ) as pool:
                output = list(
                    tqdm(pool.imap(extract_single_value_poly_star, starmap), total=len(polygon_data_to_geo_id))
                )
        else:
            with Pool(
                settings.multiprocessing_cores,
                initializer=multiprocessing_initialize,
                initargs=(settings_vals, tif_file.path, tif_file.crs),
            ) as pool:
                output = list(
                    tqdm(pool.imap(extract_single_value_poly_star, starmap), total=len(polygon_data_to_geo_id))
                )
    else:
        for polygon_data, geo_id in tqdm(polygon_data_to_geo_id.items()):
            try:
                output.append(extract_single_value_poly(tif_file, geo_id, polygon_data))
            except Exception:
                error(f'Could not extract data for line. Geo ID: {geo_id}. Reason:\n"{traceback.format_exc()}".')

    output_by_geo_id = {single_result.geo_id: single_result for single_result in output}

    real_output = [
        replace(output_by_geo_id[polygon_data_to_geo_id[geo_id_to_polygon[geo_id]]], geo_id=geo_id)
        for geo_id in geo_id_to_polygon
        if polygon_data_to_geo_id[geo_id_to_polygon[geo_id]] in output_by_geo_id
    ]

    return real_output


def extract_data_from_tiffile(
    tif_file_path: Path, task_file: Path, radius: int = 0, crs: Optional[Proj] = None
) -> list[SingleTifResult]:
    """

    Parameters
    ----------
    tif_file_path: Path
        Path to a tif file.
    task_file: Path
        Path to a task file.
    radius: int
        Radius for the circular search area around each coordinate.
        If the task file uses polygons, this parameter is ignored.
    crs: Optional[Proj]
        Specifies the projection for the Geotiff file.
        You may be asked to provide this if it can't be determined automatically.

    Returns
    -------
    List[SingleTifResult]
    List of extracted values per geo_id using the SingleTifResult datastructure.
    """
    tif_file = build_tiffile(tif_file_path, crs)

    return extract_data_from_prepared_geotiff(tif_file, task_file, radius)


def extract_data_from_tiffile_and_write_output(
    tif_file_path: Path,
    task_file: Path,
    output_dir: Path,
    radius: int = 0,
    crs: Optional[Union[Proj, str]] = None,
    output_file_name: Optional[str] = None,
) -> None:
    """
    For a given set of search areas (usually locations from a task file + radius) and a geotiff file, extract average
    value from the pixels in the geotiff that lie inside each search area, as well as other metrics such as standard
    deviation.
    Then, save them to output files in a specified directory.

    Parameters
    ----------
    tif_file_path: Path
        Path to a tif file.
    task_file: Path
        Path to a task file.
    output_dir: Path
        Path to the intended output directory.
    radius: int
        Radius for the circular search area around each coordinate.
        If the task file uses polygons, this parameter is ignored.
    crs: Optional[Proj]
        Specifies the projection for the Geotiff file.
        You may be asked to provide this if it can't be determined automatically.
    output_file_name: Optional[str]
        Specify your own output file name.
        Defaults to output_{radius}.csv.
    """
    if isinstance(crs, str):
        crs = Proj(crs)

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    shutil.copyfile(task_file, os.path.join(output_dir, os.path.basename(task_file)))

    output = extract_data_from_tiffile(tif_file_path, task_file, radius, crs)

    if not output:
        raise RuntimeError("Empty output")

    if output_file_name is None:
        output_file_name = f"output_{radius}.csv"
    outfile = os.path.join(output_dir, output_file_name)

    with open(outfile, "w") as output_file:
        output_file.write(",".join(output[0].all_field_names()) + "\n")

        for tif_results in output:
            output_file.write(",".join(tif_results.as_str_dict().values()) + "\n")


def main(args: list[str]) -> int:
    """
    Main function to run Generalized Geotiff Extractor as a standalone tool.

    Parameters
    ----------
    args : List[str]
        Input arguments. Supported arguments:
        - An arg starting with "[" and ending with "]" will be interpreted as a list of target radii.
        - A numeric arg will be interpreted as a singular target radius.
        - A file path ending in .tif or .geotif will be interpreted as the location of the geotiff file.
        - An arg starting with "crs=" will be interpreted as an EGPS code.
        - An arg that is a path to a file (and isn't a .tif or .geotif) will be interpreted as the task file location.

    Returns
    -------
    int
        Python return status code
    """
    tif_file_path: Optional[str] = None

    default_task_file = "task.txt"

    radii = [100, 200, 500, 1000, 2000]
    assumed_crs = None

    for arg in args:
        if arg.startswith("[") and arg.endswith("]"):
            radii = ast.literal_eval(arg)

        elif arg.isnumeric():
            radii = [int(arg)]

        elif ".tif" in arg or ".geotif" in arg:
            tif_file_path = arg

        elif arg.startswith("crs="):
            assumed_crs = Proj(arg[4:])

        elif os.path.isfile(arg):
            default_task_file = arg

        else:
            print(f"Could not find file {arg}.")
            return -1

    if not os.path.isfile(default_task_file):
        print(f"No task file was specified, and {default_task_file} was not found. Don't know what to work on.")
        return -1

    if tif_file_path is None or not os.path.isfile(tif_file_path):
        print(f'No tif file path was specified, or specified file "{tif_file_path}" doesn\'t exist.')
        return -1

    tif_file = Path(tif_file_path)
    task_file = Path(default_task_file)
    output_dir = Path("outputs", tif_file.stem + "_" + datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

    for radius in radii:
        extract_data_from_tiffile_and_write_output(tif_file, task_file, output_dir, radius, assumed_crs)

    return 0


if __name__ == "__main__":
    main(sys.argv[1:])
