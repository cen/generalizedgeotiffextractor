import math
from abc import abstractmethod
from collections import Counter, defaultdict
from dataclasses import Field, dataclass, field, fields
from typing import Optional, Union, cast

import numpy as np

from geotiff_extractor import settings
from geotiff_extractor.utils import pprint_float


@dataclass
class SingleTifResult:
    geo_id: str

    @classmethod
    def get_keys(cls) -> tuple[Field, ...]:
        return fields(cls)

    @abstractmethod
    def as_str_dict(self) -> dict[str, str]:
        pass

    @classmethod
    def all_field_names(cls) -> list[str]:
        return ["geo_id", *cls._all_field_names_except_geo_id()]

    @classmethod
    @abstractmethod
    def _all_field_names_except_geo_id(cls) -> list[str]:
        pass


@dataclass
class MultiValueTifResult(SingleTifResult):
    geo_id: str
    total_pixel_amount: int = 0
    invalid_pixel_amount: int = 0
    area: float = 0.0
    used_area: float = 0.0
    resolution: float = 0.0

    @staticmethod
    def correct_subclass() -> type["MultiValueTifResult"]:
        if settings.classes is None:
            return AverageTifResult
        else:
            return ClassResult

    @property
    def used_pixel_amount(self) -> int:
        return self.total_pixel_amount - (0 if settings.invalid_as_0 else self.invalid_pixel_amount)

    def as_str_dict(self) -> dict[str, str]:
        return {
            "geo_id": self.geo_id,
            "pixel_amount": str(self.total_pixel_amount),
            "used_pixel_amount": str(self.used_pixel_amount),
            "invalid_pixel_amount": str(self.invalid_pixel_amount),
            "invalid_as_0": str(settings.invalid_as_0),
            "resolution": pprint_float(self.resolution, False) if self.resolution else ".",
            "area": pprint_float(self.area),
            "used_area": pprint_float(self.used_area),
        } | self.values_as_string_dict()

    @classmethod
    def _all_field_names_except_geo_id(cls) -> list[str]:
        return [
            "pixel_amount",
            "used_pixel_amount",
            "invalid_pixel_amount",
            "invalid_as_0",
            "resolution",
            "area",
            "used_area",
            *cls.correct_subclass().get_unique_field_names(),
        ]

    @staticmethod
    def from_values(
        geo_id: str,
        total_pixel_amount: int,
        invalid_pixel_amount: int,
        area: float,
        used_area: float,
        resolution: float,
        values: np.ndarray,
        weights: Optional[np.ndarray] = None,
        value_sum: Optional[Union[int, float]] = None,
    ) -> "MultiValueTifResult":
        return MultiValueTifResult.correct_subclass().from_values(
            geo_id,
            total_pixel_amount,
            invalid_pixel_amount,
            area,
            used_area,
            resolution,
            values,
            weights,
            value_sum,
        )

    def values_as_string_dict(self) -> dict[str, str]:
        return {}

    @classmethod
    def get_unique_field_names(cls) -> list[str]:
        return MultiValueTifResult.correct_subclass().get_unique_field_names()


@dataclass
class AverageTifResult(MultiValueTifResult):
    mean: float = 0.0
    stddev: float = 0.0
    sum: float = 0.0
    min: float = 0.0
    max: float = 0.0
    median: float = 0.0

    def values_as_string_dict(self) -> dict[str, str]:
        return {
            "mean": pprint_float(self.mean) if self.used_pixel_amount >= 1 else ".",
            "stddev": pprint_float(self.stddev) if self.used_pixel_amount >= 2 else ".",
            "sum": pprint_float(self.sum) if self.used_pixel_amount >= 1 else ".",
            "min": pprint_float(self.min) if self.used_pixel_amount >= 1 else ".",
            "max": pprint_float(self.max) if self.used_pixel_amount >= 1 else ".",
            "median": pprint_float(self.median) if self.used_pixel_amount >= 1 else ".",
        }

    @classmethod
    def get_unique_field_names(cls) -> list[str]:
        return ["mean", "stddev", "sum", "min", "max", "median"]

    @staticmethod
    def from_values(
        geo_id: str,
        total_pixel_amount: int,
        invalid_pixel_amount: int,
        area: float,
        used_area: float,
        resolution: float,
        values: np.ndarray,
        weights: Optional[np.ndarray] = None,
        value_sum: Optional[Union[int, float]] = None,
    ) -> MultiValueTifResult:
        if (weights is not None and not np.sum(weights)) or not len(values):
            mean, stddev, min_value, max_value, median = 0.0, 0.0, 0.0, 0.0, 0.0
        elif weights is not None:
            mean = np.average(values, weights=weights)
            if np.count_nonzero(weights) < 2:
                stddev = 0.0
            else:
                variance = np.average(np.square(values - mean), weights=weights)
                stddev = math.sqrt(variance)

            if np.count_nonzero(weights) >= 2:
                weights = weights.flatten()
                values = values.flatten()

                i = np.argsort(values)
                c = np.cumsum(weights[i])
                q = np.searchsorted(c, 0.5 * c[-1])
                if q == len(values) - 1:
                    median = values[i[-1]]
                else:
                    median = np.where(c[q] / c[-1] == 0.5, 0.5 * values[i[q]] + 0.5 * values[i[q + 1]], values[i[q]])
            elif np.count_nonzero(weights) == 1:
                median = np.sum(values[weights > 0])
            else:
                median = 0.0

            min_value = np.min(values[weights > 0])
            max_value = np.max(values[weights > 0])
        else:
            if np.count_nonzero(values) < 2:
                stddev = 0.0
            else:
                stddev = round(np.std(values), 5)

            mean, min_value, max_value, median = np.mean(values), np.min(values), np.max(values), np.median(values)

        return AverageTifResult(
            geo_id,
            total_pixel_amount,
            invalid_pixel_amount,
            area,
            used_area,
            resolution,
            mean,
            stddev,
            np.sum(values) if value_sum is None else value_sum,
            min_value,
            max_value,
            median,
        )


@dataclass
class ClassResult(MultiValueTifResult):
    percentages: dict[int, float] = field(default_factory=dict)

    def values_as_string_dict(self) -> dict[str, str]:
        assert settings.classes is not None, "A ClassResult tried to be made despite settings.classes being None"
        unknown_class_keys = [class_key for class_key in self.percentages if class_key not in settings.classes]
        assert (
            not unknown_class_keys
        ), f"Found unknown class keys {unknown_class_keys}. Known classes: {settings.classes}"

        if sum(self.percentages.values()):
            return {
                class_name: pprint_float(self.percentages.get(key, 0)) for key, class_name in settings.classes.items()
            }
        else:
            return {class_name: "." for key, class_name in settings.classes.items()}

    @classmethod
    def get_unique_field_names(cls) -> list[str]:
        assert settings.classes is not None, "A ClassResult tried to be made despite settings.classes being None"
        return [*settings.classes.values()]

    @staticmethod
    def from_values(
        geo_id: str,
        total_pixel_amount: int,
        invalid_pixel_amount: int,
        area: float,
        used_area: float,
        resolution: float,
        values: np.ndarray,
        weights: Optional[np.ndarray] = None,
        _: Optional[int | float | None] = None,
    ) -> MultiValueTifResult:
        if weights is None:
            counts = Counter(values)
            total = counts.total()

            if not total:
                percentages = {}
            else:
                percentages = {class_key: count / total for class_key, count in counts.items()}

            return ClassResult(
                geo_id,
                total_pixel_amount,
                invalid_pixel_amount,
                area,
                used_area,
                resolution,
                percentages,
            )
        weights_sum = np.sum(weights)
        if weights_sum == 0:
            percentages = {}
        else:
            weights /= np.sum(weights)
            percentages = defaultdict(int)
            for value, weight in zip(values.flat, weights.flat):
                percentages[value] += weight

        return ClassResult(
            geo_id,
            total_pixel_amount,
            invalid_pixel_amount,
            area,
            used_area,
            resolution,
            percentages,
        )


@dataclass
class ExactValueResult(SingleTifResult):
    resolution: float = 0.0
    exact_value: Optional[int | float] = None

    def as_str_dict(self) -> dict[str, str]:
        exact_value_type = "exact class" if settings.classes else "exact value"

        if self.exact_value is None:
            exact_value_string = "Invalid / Outside Boundary"
        else:
            if settings.classes:
                exact_value_string = settings.classes.get(cast(int, self.exact_value), "Unknown class key")
            else:
                exact_value_string = pprint_float(self.exact_value)

        return {
            "geo_id": self.geo_id,
            "resolution": pprint_float(self.resolution, False) if self.resolution else ".",
            exact_value_type: exact_value_string,
        }

    @classmethod
    def _all_field_names_except_geo_id(cls) -> list[str]:
        exact_value_type = "exact class" if settings.classes else "exact value"
        return ["resolution", exact_value_type]
