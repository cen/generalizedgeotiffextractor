import math

import numpy as np
import shapely.ops
import zarr
from enpgt_task_file_reader import PolygonData
from numpy import ndarray
from pyproj import Proj
from shapely import MultiPoint, Point, Polygon

import geotiff_extractor.settings as settings
from geotiff_extractor.results_dataclasses import ExactValueResult, MultiValueTifResult, SingleTifResult
from geotiff_extractor.tif_dataclasses import GeoTiffData
from geotiff_extractor.utils import (
    CRS_WGS84,
    cached_transproj,
    print_once,
    projected_polygon_data,
)


def get_exact_value(geo_id: str, tif_file: GeoTiffData, point: Point) -> ExactValueResult:
    correct_x = round((point.x - tif_file.top_left_x_center) / tif_file.pixel_width)
    correct_y = round((tif_file.top_left_y_center - point.y) / tif_file.pixel_height)

    if isinstance(tif_file.value_array, zarr.core.Array):
        exact_value = tif_file.value_array[(correct_y, correct_x)]
    else:
        exact_value = tif_file.value_array[correct_y, correct_x]

    if tif_file.pixel_is_invalid(exact_value):
        if settings.invalid_as_0:
            exact_value = 0
        else:
            exact_value = None

    if tif_file.crs == CRS_WGS84:
        left_x = tif_file.top_left_x_center + (correct_x - 0.5) * tif_file.pixel_width
        right_x = left_x + tif_file.pixel_width
        top_y = tif_file.top_left_y_center - (correct_y - 0.5) * tif_file.pixel_height
        bottom_y = top_y - tif_file.pixel_height

        pixel_polygon = Polygon(
            [
                (left_x, top_y),
                (right_x, top_y),
                (right_x, bottom_y),
                (left_x, bottom_y),
            ]
        )

        aea_proj = Proj(
            f"+proj=aea +lat_1={bottom_y} +lat_2={top_y} +lat_0={top_y - (top_y - bottom_y) / 2}"
            f" +lon_0={right_x - (right_x - left_x) / 2}"
        )
        transproj = cached_transproj(CRS_WGS84, aea_proj)

        resolution = shapely.ops.transform(transproj.transform, pixel_polygon).area
    else:
        resolution = tif_file.pixel_width * tif_file.pixel_height

    if settings.treat_value_as_per_n_square_meters:
        exact_value /= resolution
        exact_value *= settings.treat_value_as_per_n_square_meters

    return ExactValueResult(geo_id, resolution, exact_value)


def get_local_values(tif_file: GeoTiffData, polygon_data: PolygonData) -> tuple[int, int, int, int, np.ndarray]:
    min_x, min_y, max_x, max_y = polygon_data.polygon.bounds
    min_x_pixel, max_x_pixel, min_y_pixel, max_y_pixel = (
        round((min_x - tif_file.top_left_x_center) / tif_file.pixel_width) - 1,
        round((max_x - tif_file.top_left_x_center) / tif_file.pixel_width) + 1,
        round((tif_file.top_left_y_center - max_y) / tif_file.pixel_height) - 1,
        round((tif_file.top_left_y_center - min_y) / tif_file.pixel_height) + 1,
    )

    local_values = tif_file.value_array[min_y_pixel : max_y_pixel + 1][:, min_x_pixel : max_x_pixel + 1]
    zarray_shape = tif_file.value_array.shape
    if min_x_pixel < 0 or min_y_pixel < 0 or max_x_pixel >= zarray_shape[1] or max_y_pixel >= zarray_shape[0]:
        raise RuntimeError(
            "This coordinate is too close to the edge of the tif boundaries."
            "In the future, this case might be able to be handled more gracefully.\n"
            f"Zarray shape: {tif_file.value_array.shape}\n"
            f"Desired pixels: x = [{min_x_pixel}, {max_x_pixel}], y = [{min_y_pixel}, {max_y_pixel}].\n"
        )

    expected_shape = (max_y_pixel - min_y_pixel + 1, max_x_pixel - min_x_pixel + 1)
    if local_values.shape != expected_shape:
        raise RuntimeError(
            "When getting values from the zarray, the resulting array had an unexpected shape.\n"
            f"Expected shape: {expected_shape}.\n"
            f"Got: {local_values.shape}.\n"
            f"Zarray shape: {tif_file.value_array.shape}\n"
            f"Desired pixels: x = [{min_x_pixel}, {max_x_pixel}], y = [{min_y_pixel}, {max_y_pixel}].\n"
        )

    return min_x_pixel, min_y_pixel, max_x_pixel, max_y_pixel, local_values.copy()


def equal_area_fast_mode_from_values(
    geo_id: str, tif_file: GeoTiffData, polygon_data: PolygonData, values: ndarray
) -> MultiValueTifResult:
    total_pixels = len(values)

    invalid_values = tif_file.pixels_are_invalid(values)
    valid_values = np.logical_not(invalid_values)
    num_invalid_pixels = np.sum(invalid_values)

    if settings.invalid_as_0:
        values = np.multiply(np.nan_to_num(values), valid_values)
    else:
        values = values[valid_values]

    resolution = tif_file.pixel_width * tif_file.pixel_height

    if settings.treat_value_as_per_n_square_meters:
        values /= resolution / settings.treat_value_as_per_n_square_meters

    return MultiValueTifResult.from_values(
        geo_id,
        total_pixels,
        num_invalid_pixels,
        polygon_data.polygon.area,
        len(values) * tif_file.pixel_width * tif_file.pixel_height,
        resolution,
        values,
    )


def equal_area_fast_mode(geo_id: str, tif_file: GeoTiffData, polygon_data: PolygonData) -> MultiValueTifResult:
    area_poly = polygon_data.polygon
    min_x_pixel, min_y_pixel, max_x_pixel, max_y_pixel, local_values = get_local_values(tif_file, polygon_data)

    x_values = np.arange(min_x_pixel, max_x_pixel + 1)
    y_values = np.arange(min_y_pixel, max_y_pixel + 1)

    x = tif_file.top_left_x_center + x_values * tif_file.pixel_width
    y = tif_file.top_left_y_center - y_values * tif_file.pixel_width

    a = np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))])

    b = local_values.reshape(a.shape[0], 1)

    full = np.append(a, b, axis=1)  # TODO: ACTUALLY MAKE SURE THESE ARE THE SAME y->x ORDER!

    m = MultiPoint(full)
    m = area_poly.intersection(m)

    values = np.fromiter((p.z for p in m.geoms), tif_file.dtype)

    return equal_area_fast_mode_from_values(geo_id, tif_file, polygon_data, values)


def equal_area_radius_mode(geo_id: str, tif_file: GeoTiffData, polygon_data: PolygonData) -> MultiValueTifResult:
    x, y = polygon_data.original_x, polygon_data.original_y
    radius = polygon_data.radius

    min_x_pixel, min_y_pixel, max_x_pixel, max_y_pixel, local_values = get_local_values(tif_file, polygon_data)

    x_pixels = np.arange(min_x_pixel, max_x_pixel + 1)
    y_pixels = np.arange(min_y_pixel, max_y_pixel + 1)

    x_values = tif_file.top_left_x_center + x_pixels * tif_file.pixel_width
    y_values = tif_file.top_left_y_center - y_pixels * tif_file.pixel_width

    delta_x_squared = np.square(x_values - x)
    delta_y_squared = np.square(y_values - y)

    y_transposed = delta_y_squared.reshape((-1, 1))
    distance = np.tile(delta_x_squared, (len(delta_y_squared), 1)) + y_transposed

    values = local_values[distance <= radius**2]

    ret = equal_area_fast_mode_from_values(geo_id, tif_file, polygon_data, values)
    ret.area = radius * radius * math.pi
    return ret


def general_mode(
    geo_id: str, tif_file: GeoTiffData, polygon_data: PolygonData, fast: bool = False
) -> MultiValueTifResult:
    area_poly = polygon_data.polygon
    min_x, min_y, max_x, max_y = area_poly.bounds

    min_x_pixel, min_y_pixel, max_x_pixel, max_y_pixel, local_values = get_local_values(tif_file, polygon_data)

    if tif_file.crs == CRS_WGS84:
        aea_proj = Proj(
            f"+proj=aea +lat_1={min_y} +lat_2={max_y} +lat_0={max_y - (max_y - min_y) / 2}"
            f" +lon_0={max_x - (max_x - min_x) / 2}"
        )
        transproj = cached_transproj(CRS_WGS84, aea_proj)

        # Project area poly to local equal area projection if WGS84 mode
        projected_area_poly = shapely.ops.transform(transproj.transform, area_poly)

        polygon_points = [
            [
                transproj.transform(
                    tif_file.top_left_x_center + (x - 0.5) * tif_file.pixel_width,
                    tif_file.top_left_y_center - (y - 0.5) * tif_file.pixel_height,
                )
                for x in range(min_x_pixel, max_x_pixel + 2)
            ]
            for y in range(min_y_pixel, max_y_pixel + 2)
        ]

        middle_y = len(polygon_points) // 2
        middle_x = len(polygon_points[0]) // 2
        resolution = Polygon(
            [
                polygon_points[middle_y - 1][middle_x - 1],
                polygon_points[middle_y][middle_x - 1],
                polygon_points[middle_y][middle_x],
                polygon_points[middle_y - 1][middle_x],
            ]
        ).area
    else:
        projected_area_poly = area_poly
        polygon_points = [
            [
                Point(
                    tif_file.top_left_x_center + (x - 0.5) * tif_file.pixel_width,
                    tif_file.top_left_y_center - (y - 0.5) * tif_file.pixel_height,
                )
                for x in range(min_x_pixel, max_x_pixel + 2)
            ]
            for y in range(min_y_pixel, max_y_pixel + 2)
        ]
        resolution = tif_file.pixel_width * tif_file.pixel_height

    if fast:
        containings = np.array(
            [
                [
                    area_poly.contains(
                        Point(
                            tif_file.top_left_x_center + x * tif_file.pixel_width,
                            tif_file.top_left_y_center - y * tif_file.pixel_height,
                        )
                    )
                    for x in range(min_x_pixel, max_x_pixel + 1)
                ]
                for y in range(min_y_pixel, max_y_pixel + 1)
            ]
        )

        areas_non_intersected = np.array(
            [
                [
                    shapely.Polygon(
                        [
                            polygon_points[y][x],
                            polygon_points[y + 1][x],
                            polygon_points[y + 1][x + 1],
                            polygon_points[y][x + 1],
                        ]
                    ).area
                    for x in range(max_x_pixel - min_x_pixel + 1)
                ]
                for y in range(max_y_pixel - min_y_pixel + 1)
            ]
        )

        areas = np.multiply(areas_non_intersected, containings)

        area_percentages = areas != 0
    else:
        polygons = [
            [
                shapely.Polygon(
                    [
                        polygon_points[y][x],
                        polygon_points[y + 1][x],
                        polygon_points[y + 1][x + 1],
                        polygon_points[y][x + 1],
                    ]
                )
                for x in range(max_x_pixel - min_x_pixel + 1)
            ]
            for y in range(max_y_pixel - min_y_pixel + 1)
        ]

        areas = np.array(
            [
                [
                    shapely.intersection(
                        polygons[y][x],
                        projected_area_poly,
                    ).area
                    for x in range(max_x_pixel - min_x_pixel + 1)
                ]
                for y in range(max_y_pixel - min_y_pixel + 1)
            ]
        )

        areas_non_intersected = np.array(
            [
                [polygons[y][x].area for x in range(max_x_pixel - min_x_pixel + 1)]
                for y in range(max_y_pixel - min_y_pixel + 1)
            ]
        )

        area_percentages = np.divide(areas, areas_non_intersected)

    invalid_pixels = np.logical_or(
        tif_file.pixels_are_invalid(local_values),
        np.isnan(local_values),
    )

    num_invalid_pixels = np.sum(invalid_pixels * (areas != 0))

    if settings.invalid_as_0:
        weights = areas
    else:
        weights = areas * np.logical_not(invalid_pixels)

    local_values = np.nan_to_num(local_values, False)
    local_values *= np.logical_not(invalid_pixels)

    value_sum = np.sum(np.multiply(local_values, area_percentages))

    if settings.treat_value_as_per_n_square_meters:
        local_values /= areas_non_intersected
        local_values *= settings.treat_value_as_per_n_square_meters

    assert resolution != 0, "Somehow, resolution is 0. That should be impossible."

    return MultiValueTifResult.from_values(
        geo_id,
        np.count_nonzero(areas),
        num_invalid_pixels,
        projected_area_poly.area,
        np.sum(areas),
        resolution,
        local_values,
        weights=weights,
        value_sum=value_sum,
    )


def extract_single_value_poly(tif_file: GeoTiffData, geo_id: str, polygon_data: PolygonData) -> SingleTifResult:
    if polygon_data is None:
        return MultiValueTifResult(geo_id)

    polygon_data = projected_polygon_data(polygon_data, tif_file.crs)

    if isinstance(polygon_data.polygon, Point):
        return get_exact_value(geo_id, tif_file, polygon_data.polygon)

    area_poly = polygon_data.polygon

    min_x, min_y, max_x, max_y = area_poly.bounds
    assert (
        tif_file.top_left_x_center < min_x < max_x < tif_file.bottom_right_x_center
    ), "Coordinate out of bounds of tif file boundaries."
    assert (
        tif_file.top_left_y_center > max_y > min_y > tif_file.bottom_right_y_center
    ), "Coordinate out of bounds of tif file boundaries."
    predicted_pixels = area_poly.area / (tif_file.pixel_width * tif_file.pixel_height)

    if predicted_pixels >= settings.fast_mode_pixels:
        print_once(
            "fast mode",
            f"Using fast mode as the predicted amount of pixels ({predicted_pixels}) "
            f"is at least {settings.fast_mode_pixels}.",
        )

        if tif_file.crs != CRS_WGS84:
            if polygon_data.radius and polygon_data.original_x is not None and polygon_data.original_y is not None:
                return equal_area_radius_mode(geo_id, tif_file, polygon_data)
            else:
                return equal_area_fast_mode(geo_id, tif_file, polygon_data)
        else:
            return general_mode(geo_id, tif_file, polygon_data, fast=True)
    else:
        print_once(
            "strict mode",
            f"Using strict mode as the predicted amount of pixels ({predicted_pixels}) is lower than 1000.",
        )
        return general_mode(geo_id, tif_file, polygon_data)
