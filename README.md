# Generalized Geotiff Extractor

Many GIS datasets come in the form of .tif files where each pixel responds to a square area in some projection.
For projects like the [ENPGT Toolbox](https://git.mpib-berlin.mpg.de/metzner/enpgt-toolbox) often want to extract the averaged out data for an area, e.g. in a radius around someone's home.

The idea behind the Generalized Geotiff Extractor is that this process is almost always going to be the same, so it'd be nice to have one unified tool that can extract that data from an arbitrary .tif-based dataset.

File format:
1. `.tif file + .tfw metadata` (Implemented ✅)
2. `GeoTIFF file with baked-in metadata` (Implemented ✅)

Coordinate systems:
1. `WGS84` (Implemented ✅)
2. `(Roughly) equidistant local projections´ (Implemented ✅)

All of these have to be as performant as possible. In some cases, we want to extract data arranged in a 10m grid with 2000m radii for thousands of base coordinates.
